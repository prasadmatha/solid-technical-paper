# **SOLID: The First 5 Principles of Object Oriented Design** ##

## Introduction: ##

**SOLID** is an acronym for the first five object-oriented design (OOD) principles by Robert C. Martin. These principles establish practices that lend to developing software with considerations for maintaining and extending as the project grows.

**SOLID stands for:**

***S - Single-responsiblity Principle***

***O - Open-closed Principle***

***L - Liskov Substitution Principle***

***I - Interface Segregation Principle***

***D - Dependency Inversion Principle***

----------------------------------------------

### **1. Single-Responsibility Principle:** ###

A class or function should have one and the only reason to change. Each class should do one thing & do it well. Instead of thinking that we should split code because it would look cleaner in a single file, we split code up based on the users' social structure. Because that's what dictates change. Few things to note:

- Don't put functions in the same class that change for various causes.

- Think responsibilities (reason to change) regarding the user who will use it.

- The class should be low coupling & high cohesive.


        class Employee {
            public calculateSalary (): number { // code...}
        public hoursWorked (): number { // code..}
        public storeToDB (): any { // code.. }
        } //Here's an SRP violation:

----------------------------------------------

### **2. Open-Closed Principle:** ###
Software entities (classes, modules, functions, and so on) should be extensible but not modifiable ( no change in old code). The above approach is based on the premise that we should be able to introduce new features without changing the present code.

Violation Open Closed Principle: a bunch of if or switch statements.

----------------------------------------------

### **3. Liskov Substitution Principle:** ###

The Liskov Substitution Principle (LSP) states that objects of a superclass should be replaceable with objects of its subclasses without breaking the application. In other words, what we want is to have the objects of our subclasses behaving the same way as the objects of our superclass.

    class Bird {  

        fly(){
            //..
        }
    }
    class Eagle extends Bird {
        dive(){
        //..
        }
    }

    const eagle = new Eagle();
    eagle.fly();
    eagle.dive();

    class Penguin extends Bird(){

        //Problem: Can't fly! 

    }
----------------------------------------------

### **4. Interface Segregation Principle:** ###

Clients should not be pushed to employ interfaces that they are unfamiliar with or they don't want to use.
This approach aims to reduce the negative consequences of using large interfaces by breaking them down into smaller ones. It's similar to the Single Responsibility Principle, which asserts that any class or interface should be used for only one purpose.

----------------------------------------------

### **5. Dependency Inversion Principle:** ###

Dependency: When one class is used inside another. As a result, our class is reliant on another. Your code should be based on abstraction rather than implementation.
Low-level modules should not be relied upon by high-level modules. Abstractions should be used in both cases.

    class GooglePayService {

        constructor (googlePayInstance) {

            this.gps = googlePayInstance;

        }
        pay (to, amount ) {
        }
    }

10 months later, if your manager tells they want to change payment service to PhonePay instead of GooglePay or use both, your code should be scalable for extension.

-----------------------------------------------------------

## **References**: ##

- https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design


- http://surl.li/cjfje

- http://surl.li/cjfnm



